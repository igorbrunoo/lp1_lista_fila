#ifndef FILA_H_
#define FILA_H_

template<class T>
class Fila : public Lista{
private:
	Lista<T> * m_elementos;

public:
	Fila();
	~Fila();
	T inserir();
	T remover();
	T vazio();
	T cheio();
};

template<class T>
Fila<T>::Fila(){

}

template<class T>
Fila<T>::~Fila(){
	
}

template<class T>
T Fila<T>::total(){

}

template<>
int Fila<string>::total(){

}

T addElementos(){

}

#endif;